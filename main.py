# Scope of work:
# - create an interface that offers the user a menu of 5 choices:
# -- 1. Add a box type
# -- 2. Show box types
# -- 3. Load box to container
# -- 4. Show containers
# -- 5. Summary Report
# -- X. Close

# - for now, each choice should print a simple statement indicating
# the choice, eg "Choice 2 selected"

# - the menu should be offered continuously until the user chooses X,
# in which case, the interface greets the user and the
# program terminates


def main_menu():
    menu_text = """Choose one of the below options:
1. Add a box type
2. Show box types
3. Load box to container"
4. Show containers
5. Summary Report
X. Close\n"""

    choice = ""

    while choice.upper() != "X":
        print(menu_text)

        choice = input("Your choice: ")

        if choice == "1":
            print("Add a box type")
        elif choice == "2":
            print("Show box types")
        elif choice == "3":
            print("Load box to container")
        elif choice == "4":
            print("Show containers")
        elif choice == "5":
            print("Summary Report")

        print("\n")

if __name__ == "__main__":
    main_menu()

